# muse2git

Keep a textual change log for scores.

By default, MuseScore uses a compressed format that makes it not suitable for finding differences and reverting changes.
It is not rare that a file gets corrupted without any chance to get it back.


## Pre-requisite

- MuseScore **3.x** must be installed with RPM. https://koji.fedoraproject.org/koji/packageinfo?packageID=7901 
- MuseScore **4.x** must be installed with Flatpak. https://flathub.org/apps/org.musescore.MuseScore


The Scores are expected to be in `${HOME}/MusicProd/MuseScore{3,4}/`


## Usage

Run: `./export-{3,4}.sh`

Logs are kept in `mscore-{3,4}.log`


## TODO

- During the conversion multiple errors appears.  Investigate
- commit automatically? 


## DONE

- Exports both `.mscx` (MuseScore XML proprietary format) and `.musicxml` formats (MusicXML open standard)
- Remove the `<encoding-date>` field from MusicXML, otherwise they are always different. (This element is optional https://w3c.github.io/musicxml/)
