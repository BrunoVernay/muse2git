#!/usr/bin/env sh
set -u

# See
#   https://musescore.org/en/handbook/3/command-line-options
# Note: musicxml or xml are uncompressed MusicXML file
# mscore <>.mscz --export-to <>.mscx
# Note: mscore redirects everything to the error console.


MSCORE_3=$(which mscore)
LIST="Ideas Work Jazz Duo Pop Transcript Guitar Grooves Drum"
SCORES=${HOME}/MusicProd/MuseScore3/Scores
MSCORE_3_LOG=$(pwd)/mscore-3.log

# Clean up
rm $MSCORE_3_LOG

# Create the folders
crdir(){
    echo "$1" |\
    sed "s|$2/||1" -z |\
    xargs -I{} mkdir -p {}
}
export -f crdir

# Do the conversion
cnv_msc(){
    target=$(echo "$1" | sed "s|$2/||1 ; s|.mscz$||")
    $3 "$1" --export-to "$target".mscx >> $4 2>&1 
    $3 "$1" --export-to "$target".musicxml >/dev/null 2>&1
    sed -i '/encoding-date/d' "$target".musicxml 
}
export -f cnv_msc


echo -e "\nClean up"
rm -rf ms3 ; mkdir ms3 ; cd ms3


echo "Starting ..."
echo "# Using $(rpm -q mscore) on $(date --iso-8601)." > $MSCORE_3_LOG
# mscore long-version is bugged:
$MSCORE_3 --long-version >> $MSCORE_3_LOG 2>&1 
echo "# " >> $MSCORE_3_LOG

for subd in $LIST
do
    echo "- Working on $subd ... "
    find ${SCORES}/$subd -type d ! -name '*backup*' -print0 |\
    xargs -0 -I{} sh -c 'crdir "$@"' _ {} ${SCORES}

    find ${SCORES}/$subd -type f -name '*.mscz' -print0 |\
    xargs -0 -I{} sh -c 'cnv_msc "$@"' _ {} ${SCORES} $MSCORE_3 $MSCORE_3_LOG

done 

echo -e "Finished.\n"

# Clean the logs ...
sed -i "s/\t// ; s/\.\.\.$/ / ; s|$SCORES|| ; /^QSocketNotifier/d ; /^Creating main window/d ; /^Reading translations/d" $MSCORE_3_LOG
# Not interested in specific errors (All unknown lines are errors)
sed -i '4,${/^convert \|^to \|^\.\.\. /!s/.*/WithErrors/}' $MSCORE_3_LOG
# Remove duplicated lines
sed -i '$!N; /^\(.*\)\n\1$/!P; D' $MSCORE_3_LOG
 # if a line begins with "..." or "to " or WithErrors  append it to the previous line
sed -i -e :a -e '$!N;s/\n\.\.\. / /;ta' -e 'P;D' $MSCORE_3_LOG
sed -i -e :a -e '$!N;s/\nto /to /;ta' -e 'P;D' $MSCORE_3_LOG
sed -i -e :a -e '$!N;s/\nWithErrors/ WithErrors /;ta' -e 'P;D' $MSCORE_3_LOG

echo "Converted $(grep mscx $MSCORE_3_LOG | grep -c success) files to mscx."
echo "There was $(grep -c WithErrors $MSCORE_3_LOG) files with errors."
echo "Failures: $(grep convert $MSCORE_3_LOG | grep -c -v success) "
grep convert $MSCORE_3_LOG | grep -v success
echo "Errors (that did not trigger a conversion failure):"
grep WithErrors $MSCORE_3_LOG 
