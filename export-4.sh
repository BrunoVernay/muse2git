#!/usr/bin/env sh
set -u

# See
#   https://musescore.org/en/handbook/4/command-line-usage
# Note: musicxml or xml are uncompressed MusicXML file
# mscore <>.mscz --export-to <>.mscx
# Note: mscore redirects everything to the error console.


MSCORE_4="flatpak run org.musescore.MuseScore"
LIST="Ideas Work Jazz Duo DuoPiano Pop Trad Transcript Guitar Grooves Drum"
SCORES=${HOME}/MusicProd/MuseScore4/Scores
MSCORE_4_LOG=$(pwd)/mscore-4.log


# Create the folders
crdir(){
    echo "$1" |\
    sed "s|$2/||1" -z |\
    xargs -I{} mkdir -p {}
}
export -f crdir

# Do the conversion
cnv_msc(){
    target=$(echo "$1" | sed "s|$2/||1 ; s|.mscz$||")
    mkdir "$target" ; cd "$target"
    $4 "$1" --export-to "$(echo "$target" | sed "s|$3/||1")".mscx >> $5 2>&1 
    cd -
    $4 "$1" --export-to "$target".musicxml >> $5 2>&1
    sed -i '/encoding-date/d' "$target".musicxml 
}
export -f cnv_msc


echo -e "\nClean up"
rm -rf ms4 ; mkdir ms4 ; cd ms4

echo "Starting ... "
echo "# $(date --iso-8601) ; Using: $(flatpak info org.musescore.MuseScore)" > $MSCORE_4_LOG
echo "#" >> $MSCORE_4_LOG

for subd in $LIST
do
    echo "- Working on $subd ... "
    find ${SCORES}/$subd -type d ! -name '*backup*' -print0 |\
    xargs -0 -I{} sh -c 'crdir "$@"' _ {} ${SCORES}

    find ${SCORES}/$subd -type f -name '*.mscz' -print0 |\
    xargs -0 -I{} sh -c 'cnv_msc "$@"' _ {} ${SCORES} "${subd}" "$MSCORE_4" "$MSCORE_4_LOG"

done 

echo -e "Finished.\n"

# Clean the logs ...
sed -i "/wl_display/d ; /^qt.qpa/d ; /^Gtk-Message/d ; /network socket$/d" $MSCORE_4_LOG

grep WithErrors $MSCORE_4_LOG 
